// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SJGameMode.h"
#include "SJHUD.h"
#include "SJCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASJGameMode::ASJGameMode()
	: Super()
{
	// use our custom HUD class
	HUDClass = ASJHUD::StaticClass();
}
