// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "GameStruct.h"
#include "InteractableObjectBase.generated.h"

UCLASS()
class SJ_API AInteractableObjectBase : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* OverlapBoxComponent;

	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh3P;

public:	
	// Sets default values for this actor's properties
	AInteractableObjectBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	int32 itemID;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetItemID() { return itemID; }

	FInventoryItem ItemInfo;

	UFUNCTION(BlueprintImplementableEvent)
	void InitializeItemInformation();

	UFUNCTION(BlueprintCallable)
	void SetItemInformation(FInventoryItem InItemInfo);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
