// Fill out your copyright notice in the Description page of Project Settings.


#include "SJCharacterBase.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "InteractableObjectBase.h"
#include "SJHUD.h"
#include "ItemInterface.h"

// Sets default values
ASJCharacterBase::ASJCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->TargetArmLength = 200.0f;
	SpringArmComponent->SocketOffset = FVector(0, 35, 0);
	SpringArmComponent->TargetOffset = FVector(0, 0, 55);
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->SetupAttachment(GetRootComponent());
	 
	// Create a CameraComponent	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	TraceIgnoreActors.Reserve(5);
}

// Called when the game starts or when spawned
void ASJCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
	TraceIgnoreActors.Add(this);
}

// Called every frame
void ASJCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

// Called to bind functionality to input
void ASJCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ASJCharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASJCharacterBase::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.

	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASJCharacterBase::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASJCharacterBase::LookUpAtRate);

	PlayerInputComponent->BindAction("Pickup", IE_Pressed, this, &ASJCharacterBase::PickupItem);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASJCharacterBase::OnCrouch);

}

void ASJCharacterBase::MoveForward(float Val)
{
	if (Val != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Val);
	}
}

void ASJCharacterBase::MoveRight(float Val)
{
	if (Val != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Val);
	}
}

void ASJCharacterBase::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASJCharacterBase::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASJCharacterBase::Internal_OnPickupItem(FInventoryItem ItemInfo)
{
	if (InventoryComponent)
	{
		if (InventoryComponent->AddItemToInventory(ItemInfo))
		{
			ASJHUD* MyHUD = Cast<ASJHUD>(Cast<APlayerController>(GetController())->GetHUD());

			if (MyHUD)
			{
				MyHUD->AddToInventory(ItemInfo);
			}
		}
	}
}

void ASJCharacterBase::PickupItem()
{
	OnPlay_PickupAnim();

	if (!bCanPickupItem())
	{
		ASJHUD* MyHUD = Cast<ASJHUD>(Cast<APlayerController>(GetController())->GetHUD());

		if (MyHUD)
		{
			MyHUD->FailedToAddToInventory();
		}

		return;
	}

	FInventoryItem TryPickupItem = FindInteractObject();

	if (TryPickupItem.bIsInitialized)
	{
		Internal_OnPickupItem(TryPickupItem);

		UE_LOG(LogTemp, Warning, TEXT("Found Interactable Object %s"), *TryPickupItem.ItemName);
	}
}

FInventoryItem ASJCharacterBase::FindInteractObject()
{
	FInventoryItem InteractingItem = FInventoryItem();

	FVector StartLoc = GetActorLocation();
	FVector EndLoc = (GetActorLocation());

	TArray<FHitResult> HitResults;
	ETraceTypeQuery QueryType = UEngineTypes::ConvertToTraceType(ECC_Visibility);
	bool bObjectIsUnderneath = UKismetSystemLibrary::SphereTraceMulti(GetWorld(), StartLoc, EndLoc, 100.0f, QueryType, false, TraceIgnoreActors, EDrawDebugTrace::None, HitResults, true);
	
	if (bObjectIsUnderneath)
	{
		for (FHitResult hit : HitResults)
		{
			if (hit.Actor->GetClass()->IsChildOf(AInteractableObjectBase::StaticClass()))
			{
				UE_LOG(LogTemp, Warning, TEXT("There is an item below us %s"), *hit.GetActor()->GetName());

				IItemInterface* ItemObject = Cast<IItemInterface>(hit.GetActor());

				if (ItemObject)
				{
					InteractingItem = ItemObject->GetInventoryItem();

					hit.GetActor()->SetLifeSpan(0.1f);

					return InteractingItem;
				}
			}
		}

	}

	return InteractingItem;
}

void ASJCharacterBase::OnCrouch()
{
	if (!bIsCrouched)
		Super::Crouch();
	else
		Super::UnCrouch();

	OnPlay_CrouchAnim(bIsCrouched);
}

bool ASJCharacterBase::bCanPickupItem()
{
	if (InventoryComponent)
	{
		return InventoryComponent->CanPickupItem();
	}

	return false;
}
