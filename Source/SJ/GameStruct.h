// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameStruct.generated.h"

/**
 * 
 */
UCLASS()
class SJ_API UGameStruct : public UObject
{
	GENERATED_BODY()
	
};

USTRUCT(BlueprintType, Blueprintable)
struct FInventoryItem
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString ItemDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ItemCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ItemID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bStackable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsInitialized;

	FInventoryItem() :
		bStackable(false),
		bIsInitialized(false)
	{

	}

	void InitializeInventoryItem(const FInventoryItem& InvenItem);

	bool IsItemStackable() { return bStackable; }

	bool IsItemInitialized() { return bIsInitialized; }


};
