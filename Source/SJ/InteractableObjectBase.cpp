// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableObjectBase.h"
#include "SJCharacterBase.h"

// Sets default values
AInteractableObjectBase::AInteractableObjectBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh3P"));
	RootComponent = Mesh3P;

	OverlapBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComponent"));
	OverlapBoxComponent->AttachTo(GetRootComponent());

}

// Called when the game starts or when spawned
void AInteractableObjectBase::BeginPlay()
{
	Super::BeginPlay();

	InitializeItemInformation();
	
}

void AInteractableObjectBase::SetItemInformation(FInventoryItem InItemInfo)
{
	ItemInfo = InItemInfo;
}

// Called every frame
void AInteractableObjectBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInteractableObjectBase::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor)
	{
		ASJCharacterBase* MyCharacter = Cast< ASJCharacterBase>(OtherActor);

		if (MyCharacter)
		{

		}
	}
}

