// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	InventoryList.Reserve(10);
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	UE_LOG(LogTemp, Warning, TEXT("Inventory size :%d"), InventoryList.Num());
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int32 UInventoryComponent::GetAvailableInventoryIndex()
{
	for (int32 i = 0; i < InventoryList.Num(); i++)
	{
		if (!InventoryList[i].IsItemInitialized())
		{
			return i;
		}
	}

	return -1;
}

void UInventoryComponent::AddItemToInventory(const FInventoryItem & InvenItem)
{
	int32 IndexToOccupy = GetAvailableInventoryIndex();

	if (IndexToOccupy > 0)
	{
		InventoryList[IndexToOccupy].InitializeInventoryItem(InvenItem);

		UE_LOG(LogTemp, Warning, TEXT("Added Inve to slot %d item %s"), IndexToOccupy, *InvenItem.ItemName);
	}
}

