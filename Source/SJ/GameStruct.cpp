// Fill out your copyright notice in the Description page of Project Settings.


#include "GameStruct.h"

void FInventoryItem::InitializeInventoryItem(const FInventoryItem & InvenItem)
{
	ItemName = InvenItem.ItemName;
	ItemDescription = InvenItem.ItemDescription;
	ItemCount = InvenItem.ItemCount;
	bStackable = InvenItem.bStackable;
	bIsInitialized = true;
	ItemID = InvenItem.ItemID;
}
